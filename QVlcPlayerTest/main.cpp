#include <QApplication>
#include "Player.h"
#include <VLCQtCore/Common.h>
#include <VLCQtCore/Instance.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Player player;
	player.show();

    return a.exec();
}
