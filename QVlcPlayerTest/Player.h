#pragma once

#include <QWidget>

class VlcInstance;
class VlcMedia;
class VlcMediaPlayer;

class Player : public QWidget
{
    Q_OBJECT

public:
    Player(QWidget *parent = nullptr);
    ~Player();

private:
    VlcInstance *m_pVlcIns = nullptr;
    VlcMedia *m_pVlcMedia = nullptr;
    VlcMediaPlayer *m_pVlcPlayer = nullptr;
};
