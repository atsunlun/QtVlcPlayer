#include "Player.h"

#include <VLCQtCore/Audio.h>
#include <VLCQtCore/Common.h>
#include <VLCQtCore/Instance.h>
#include <VLCQtCore/Media.h>
#include <VLCQtCore/MediaPlayer.h>
#include <VLCQtWidgets/WidgetSeek.h>
#include <VLCQtWidgets/WidgetVideo.h>
#include <VLCQtWidgets/WidgetVolumeSlider.h>
#include <QToolButton>
#include <QLayout>

#pragma execution_character_set("utf-8")

Player::Player(QWidget *parent)
    : QWidget(parent)
{
	//创建vlc实例
	m_pVlcIns = new VlcInstance(VlcCommon::args(), NULL);
	m_pVlcIns->setLogLevel(Vlc::DebugLevel);
	//libvlc是否加载
	if (!m_pVlcIns->status())
		return;
	//创建vlc播放器
	m_pVlcPlayer = new VlcMediaPlayer(m_pVlcIns);
	//创建视频控件
	VlcWidgetVideo *pVideoWidget = new VlcWidgetVideo(this);
	m_pVlcPlayer->setVideoWidget(pVideoWidget);
	pVideoWidget->setMediaPlayer(m_pVlcPlayer);
	pVideoWidget->setFixedSize(400, 400);
	//创建音量控件
	VlcWidgetVolumeSlider *pVolumeSlider = new VlcWidgetVolumeSlider(this);
	pVolumeSlider->setMediaPlayer(m_pVlcPlayer);
	pVolumeSlider->setVolume(80); //初始化音量
	//创建进度条控件
	VlcWidgetSeek *pSeekWidget = new VlcWidgetSeek(this);
	pSeekWidget->setMediaPlayer(m_pVlcPlayer);

	QVBoxLayout *pMainLayout = new QVBoxLayout(this);
	{
		
		QHBoxLayout *pControlLayout = new QHBoxLayout();
		{
			QToolButton *pPauseBtn = new QToolButton(this);
			pPauseBtn->setText("暂停");
			pPauseBtn->setCheckable(true);
			pPauseBtn->setChecked(true);
			connect(pPauseBtn, SIGNAL(toggled(bool)), m_pVlcPlayer, SLOT(togglePause()));

			pControlLayout->addWidget(pPauseBtn);
			pControlLayout->addWidget(pVolumeSlider);
			pControlLayout->addWidget(pSeekWidget);
			pControlLayout->setContentsMargins(0, 0, 0, 0);
			pControlLayout->setSpacing(0);
		}

		pMainLayout->addWidget(pVideoWidget);
		pMainLayout->addLayout(pControlLayout);
		pMainLayout->setContentsMargins(0, 0, 0, 0);
		pMainLayout->setSpacing(0);
	}
	m_pVlcMedia = new VlcMedia("http://192.168.1.178:8082/20210201/3be44269d0c742b8b8fb89705150fa9c.mp4", m_pVlcIns);
	m_pVlcPlayer->open(m_pVlcMedia);

	this->setFixedSize(600, 600);
}

Player::~Player()
{
    if (m_pVlcIns->status())
    {
		m_pVlcPlayer->stop();
        delete m_pVlcMedia;
        delete m_pVlcPlayer;
    }
    delete m_pVlcIns;
}

