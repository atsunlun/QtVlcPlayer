# QtVlcPlayer

#### 介绍
qt vlc播放器
qt5.14.2
vs2017

#### 安装教程
安装步骤查看原作者[https://blog.csdn.net/HelloEarth_/article/details/103046448](https://blog.csdn.net/HelloEarth_/article/details/103046448)

最近为了做RTMP推流，准备先拿QT+VLC学习一下，首先就是编译VLC

1. VLC-QT下载
通过官网：https：//vlc-qt.tano.si/  提供的库是QT5.6.1 + VLC2.2.4
1）源码包
直接去GITHUB下载vlc-qt.zip源码包： https://github.com/vlc-qt/vlc-qt
2）VLC库
官方是用vlc2.2.4，所以我下了的是vlc3.0.0，在下载时要分清，是32位还是64位
vlc下载地址：http://download.videolan.org/vlc/

2. 编译
1）部署VLC环境

![输入图片说明](https://images.gitee.com/uploads/images/2021/0320/170325_c07aec11_1242483.png "屏幕截图.png")

把plugins和两个dll的文件复制到QT安装目录下的bin目录中（例如：D:\Qt\Qt5.9.3\5.9.3\msvc2015\bin）
把sdk/include/vlc目录复制到QT安装目录下的include目录中（例如：D:\Qt\Qt5.9.3\5.9.3\msvc2015\include）
把sdk/lib 下面的几个LIB文件复制到QT安装目录下面的lib目录中（例如：D:\Qt\Qt5.9.3\5.9.3\msvc2015\lib）
注意这里我们要设置一个环境变量，就是我们的插件文件夹的位置，打开计算机右键添加环境变量即可，例如：
VLC_PLUGIN_PATH C:\Qt\Qt5.9.0\5.9\msvc2017_64\bin/plugins

2）生成vs2017项目
生成VS2017项目，我们需要用到CMAKE。
运行结果为下图所示，然后我们要检查各个项配置

![输入图片说明](https://images.gitee.com/uploads/images/2021/0320/170410_597e0bfe_1242483.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0320/172213_3bcc6ef2_1242483.png "屏幕截图.png")

1）makeinstall
这项表示VLC-QT的makeinstall的目录，我建议大家不要设置为系统盘下，最好改到其它盘下，因为在makeinstall时，会因为权限不够造成安装时不成功，所以我设置到了D盘目录下。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0320/170453_43d5d3eb_1242483.png "屏幕截图.png")

2）vlc
这三项是关于VLC库的配置，我们看到这里面LIBVLCCORE_LIBRARY和LIBVIC_LIBRARY的链接是不正确的，我们需要手动改一下，分别改成：libvlccore.lib和libvlc.lib

![输入图片说明](https://images.gitee.com/uploads/images/2021/0320/170509_14b19f0b_1242483.png "屏幕截图.png")

3）Add Entry
在这里我们需要添加一个变量，可以方便大家在生成库的时候，如果是debug版的可以带d，不然的话，在生成debug和release时，不是不带d的库，会覆盖掉。
如下图所示，“Add Entry”， 
名称：CMAKE_DEBUG_POSTFIX
类型：STRING
价值：d

![输入图片说明](https://images.gitee.com/uploads/images/2021/0320/170535_05287731_1242483.png "屏幕截图.png")

好了设置好后，点击configure成功后，然后执行Generate生成VS2017工程。

4）修改cmake_install.cmake
这一个操作，是在我编译过程中发现的问题，这个问题会导致后面安装时不成功。
在构建目录下找到cmake_install.cmake文件然后以文本的形式打开。如下图红框所示，两个DLL的位置配置不对，我们要手动修改一下，改为bin目录下。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0320/170552_1a86497d_1242483.png "屏幕截图.png")

5）修改vs2017属性
分别修改Core，PluginQml，Qml，Widgets工程的属性>>调试/发布>>链接器>>命令行>>其它选项下填入   /SAFESEH:NO
好了，设置以上设置后，我们可以生成动态库了，直接在ALL_BUILD工程上右击生成，就可以生成动态库了，生成成功后再在“INSTALL”工程上右击生成，就可将库提取到安装目录里了，即在CMake的的时设置的那个目录。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0320/170607_0ea254f0_1242483.png "屏幕截图.png")

就可以编译了 之后可能会报一些错误 
1，ssize_t问题。参考其他教程 说需要在 vlc.h头文件中增加( typedef __int64 ssize_t;)。本人增加后，编译过程提示ssize_t重载。说明代码中已经定义了　　　　ssize_t.（所以不需增加 typedef  __int64 ssize_t; ） 但是不加会报其他错误。本人的解决方案是吧ssize_t 类型改成__int64 此问题解决（但由于ssize_t是跨平台类型，所以程序移植后可能会出问题）。
2，poll问题 提示找不到poll标识符。 解决方案：定位到poll代码区。会看到一个poll的宏定义。需要把这个宏定义剪贴到 poll代码区的上方。
最后在install就好了，在设置的生成目录下会生成，如下图：（把以上文件夹复制到QT对应的文件夹即可使用。）

![输入图片说明](https://images.gitee.com/uploads/images/2021/0320/170647_beaaf3b6_1242483.png "屏幕截图.png")

通过以下例子可以先学习

![输入图片说明](https://images.gitee.com/uploads/images/2021/0320/170707_4d62e292_1242483.png "屏幕截图.png")

==============================================================================================
测试网址
1，RTMP协议直播源

香港卫视：rtmp://live.hkstv.hk.lxdns.com/live/hks

2，RTSP协议直播源

珠海过澳门大厅摄像头监控：rtsp://218.204.223.237:554/live/1/66251FC11353191F/e7ooqwcfbqjoo80j.sdp

大熊兔（点播）：rtsp://184.72.239.149/vod/mp4://BigBuckBunny_175k.mov

3，HTTP协议直播源

香港卫视：http://live.hkstv.hk.lxdns.com/live/hks/playlist.m3u8

CCTV1高清：http://ivi.bupt.edu.cn/hls/cctv1hd.m3u8

CCTV3高清：http://ivi.bupt.edu.cn/hls/cctv3hd.m3u8

CCTV5高清：http://ivi.bupt.edu.cn/hls/cctv5hd.m3u8

CCTV5+高清：http://ivi.bupt.edu.cn/hls/cctv5phd.m3u8

CCTV6高清：http://ivi.bupt.edu.cn/hls/cctv6hd.m3u8

苹果提供的测试源（点播）：http://devimages.apple.com.edgekey.net/streaming/examples/bipbop_4x3/gear2/prog_index.m3u8

————————————————
版权声明：本文为CSDN博主「HelloEarth_」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/HelloEarth_/article/details/103046448
