# Install script for directory: E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/widgets

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "D:/VLC-Qt")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/widgets/VLCQtWidgetsd.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/widgets/VLCQtWidgets.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/widgets/VLCQtWidgets.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/widgets/RelWithDebInfo/VLCQtWidgets.lib")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/widgets/Debug/VLCQtWidgetsd.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/widgets/Release/VLCQtWidgets.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/widgets/MinSizeRel/VLCQtWidgets.dll")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE SHARED_LIBRARY FILES "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/widgets/RelWithDebInfo/VLCQtWidgets.dll")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/VLCQtWidgets" TYPE FILE FILES
    "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/widgets/ControlAudio.h"
    "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/widgets/ControlVideo.h"
    "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/widgets/SharedExportWidgets.h"
    "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/widgets/WidgetSeek.h"
    "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/widgets/WidgetSeekProgress.h"
    "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/widgets/WidgetVideo.h"
    "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/widgets/WidgetVolumeSlider.h"
    )
endif()

