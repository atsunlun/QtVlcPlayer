/****************************************************************************
** Meta object code from reading C++ file 'QmlPlayer.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../../../../src/qml/QmlPlayer.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QmlPlayer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_VlcQmlPlayer_t {
    QByteArrayData data[43];
    char stringdata0[574];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_VlcQmlPlayer_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_VlcQmlPlayer_t qt_meta_stringdata_VlcQmlPlayer = {
    {
QT_MOC_LITERAL(0, 0, 12), // "VlcQmlPlayer"
QT_MOC_LITERAL(1, 13, 15), // "autoplayChanged"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 20), // "deinterlacingChanged"
QT_MOC_LITERAL(4, 51, 13), // "lengthChanged"
QT_MOC_LITERAL(5, 65, 15), // "logLevelChanged"
QT_MOC_LITERAL(6, 81, 15), // "positionChanged"
QT_MOC_LITERAL(7, 97, 15), // "seekableChanged"
QT_MOC_LITERAL(8, 113, 12), // "stateChanged"
QT_MOC_LITERAL(9, 126, 11), // "timeChanged"
QT_MOC_LITERAL(10, 138, 10), // "urlChanged"
QT_MOC_LITERAL(11, 149, 13), // "volumeChanged"
QT_MOC_LITERAL(12, 163, 17), // "audioTrackChanged"
QT_MOC_LITERAL(13, 181, 30), // "audioPreferredLanguagesChanged"
QT_MOC_LITERAL(14, 212, 20), // "subtitleTrackChanged"
QT_MOC_LITERAL(15, 233, 33), // "subtitlePreferredLanguagesCha..."
QT_MOC_LITERAL(16, 267, 17), // "videoTrackChanged"
QT_MOC_LITERAL(17, 285, 11), // "mediaParsed"
QT_MOC_LITERAL(18, 297, 6), // "parsed"
QT_MOC_LITERAL(19, 304, 15), // "mediaPlayerVout"
QT_MOC_LITERAL(20, 320, 5), // "count"
QT_MOC_LITERAL(21, 326, 5), // "pause"
QT_MOC_LITERAL(22, 332, 4), // "play"
QT_MOC_LITERAL(23, 337, 4), // "stop"
QT_MOC_LITERAL(24, 342, 8), // "autoplay"
QT_MOC_LITERAL(25, 351, 13), // "deinterlacing"
QT_MOC_LITERAL(26, 365, 6), // "length"
QT_MOC_LITERAL(27, 372, 8), // "logLevel"
QT_MOC_LITERAL(28, 381, 8), // "position"
QT_MOC_LITERAL(29, 390, 8), // "seekable"
QT_MOC_LITERAL(30, 399, 5), // "state"
QT_MOC_LITERAL(31, 405, 4), // "time"
QT_MOC_LITERAL(32, 410, 3), // "url"
QT_MOC_LITERAL(33, 414, 6), // "volume"
QT_MOC_LITERAL(34, 421, 15), // "audioTrackModel"
QT_MOC_LITERAL(35, 437, 14), // "VlcTrackModel*"
QT_MOC_LITERAL(36, 452, 10), // "audioTrack"
QT_MOC_LITERAL(37, 463, 23), // "audioPreferredLanguages"
QT_MOC_LITERAL(38, 487, 18), // "subtitleTrackModel"
QT_MOC_LITERAL(39, 506, 13), // "subtitleTrack"
QT_MOC_LITERAL(40, 520, 26), // "subtitlePreferredLanguages"
QT_MOC_LITERAL(41, 547, 15), // "videoTrackModel"
QT_MOC_LITERAL(42, 563, 10) // "videoTrack"

    },
    "VlcQmlPlayer\0autoplayChanged\0\0"
    "deinterlacingChanged\0lengthChanged\0"
    "logLevelChanged\0positionChanged\0"
    "seekableChanged\0stateChanged\0timeChanged\0"
    "urlChanged\0volumeChanged\0audioTrackChanged\0"
    "audioPreferredLanguagesChanged\0"
    "subtitleTrackChanged\0"
    "subtitlePreferredLanguagesChanged\0"
    "videoTrackChanged\0mediaParsed\0parsed\0"
    "mediaPlayerVout\0count\0pause\0play\0stop\0"
    "autoplay\0deinterlacing\0length\0logLevel\0"
    "position\0seekable\0state\0time\0url\0"
    "volume\0audioTrackModel\0VlcTrackModel*\0"
    "audioTrack\0audioPreferredLanguages\0"
    "subtitleTrackModel\0subtitleTrack\0"
    "subtitlePreferredLanguages\0videoTrackModel\0"
    "videoTrack"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_VlcQmlPlayer[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      20,   14, // methods
      18,  138, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      15,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  114,    2, 0x06 /* Public */,
       3,    0,  115,    2, 0x06 /* Public */,
       4,    0,  116,    2, 0x06 /* Public */,
       5,    0,  117,    2, 0x06 /* Public */,
       6,    0,  118,    2, 0x06 /* Public */,
       7,    0,  119,    2, 0x06 /* Public */,
       8,    0,  120,    2, 0x06 /* Public */,
       9,    0,  121,    2, 0x06 /* Public */,
      10,    0,  122,    2, 0x06 /* Public */,
      11,    0,  123,    2, 0x06 /* Public */,
      12,    0,  124,    2, 0x06 /* Public */,
      13,    0,  125,    2, 0x06 /* Public */,
      14,    0,  126,    2, 0x06 /* Public */,
      15,    0,  127,    2, 0x06 /* Public */,
      16,    0,  128,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      17,    1,  129,    2, 0x08 /* Private */,
      19,    1,  132,    2, 0x08 /* Private */,

 // methods: name, argc, parameters, tag, flags
      21,    0,  135,    2, 0x02 /* Public */,
      22,    0,  136,    2, 0x02 /* Public */,
      23,    0,  137,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,   18,
    QMetaType::Void, QMetaType::Int,   20,

 // methods: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      24, QMetaType::Bool, 0x00495103,
      25, QMetaType::Int, 0x00495103,
      26, QMetaType::LongLong, 0x00495001,
      27, QMetaType::Int, 0x00495103,
      28, QMetaType::Float, 0x00495103,
      29, QMetaType::Bool, 0x00495001,
      30, QMetaType::Int, 0x00495001,
      31, QMetaType::LongLong, 0x00495103,
      32, QMetaType::QUrl, 0x00495103,
      33, QMetaType::Int, 0x00495103,
      34, 0x80000000 | 35, 0x00095409,
      36, QMetaType::Int, 0x00495103,
      37, QMetaType::QStringList, 0x00495103,
      38, 0x80000000 | 35, 0x00095409,
      39, QMetaType::Int, 0x00495103,
      40, QMetaType::QStringList, 0x00495103,
      41, 0x80000000 | 35, 0x00095409,
      42, QMetaType::Int, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,
       8,
       9,
       0,
      10,
      11,
       0,
      12,
      13,
       0,
      14,

       0        // eod
};

void VlcQmlPlayer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<VlcQmlPlayer *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->autoplayChanged(); break;
        case 1: _t->deinterlacingChanged(); break;
        case 2: _t->lengthChanged(); break;
        case 3: _t->logLevelChanged(); break;
        case 4: _t->positionChanged(); break;
        case 5: _t->seekableChanged(); break;
        case 6: _t->stateChanged(); break;
        case 7: _t->timeChanged(); break;
        case 8: _t->urlChanged(); break;
        case 9: _t->volumeChanged(); break;
        case 10: _t->audioTrackChanged(); break;
        case 11: _t->audioPreferredLanguagesChanged(); break;
        case 12: _t->subtitleTrackChanged(); break;
        case 13: _t->subtitlePreferredLanguagesChanged(); break;
        case 14: _t->videoTrackChanged(); break;
        case 15: _t->mediaParsed((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 16: _t->mediaPlayerVout((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->pause(); break;
        case 18: _t->play(); break;
        case 19: _t->stop(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (VlcQmlPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlPlayer::autoplayChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (VlcQmlPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlPlayer::deinterlacingChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (VlcQmlPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlPlayer::lengthChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (VlcQmlPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlPlayer::logLevelChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (VlcQmlPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlPlayer::positionChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (VlcQmlPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlPlayer::seekableChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (VlcQmlPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlPlayer::stateChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (VlcQmlPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlPlayer::timeChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (VlcQmlPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlPlayer::urlChanged)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (VlcQmlPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlPlayer::volumeChanged)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (VlcQmlPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlPlayer::audioTrackChanged)) {
                *result = 10;
                return;
            }
        }
        {
            using _t = void (VlcQmlPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlPlayer::audioPreferredLanguagesChanged)) {
                *result = 11;
                return;
            }
        }
        {
            using _t = void (VlcQmlPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlPlayer::subtitleTrackChanged)) {
                *result = 12;
                return;
            }
        }
        {
            using _t = void (VlcQmlPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlPlayer::subtitlePreferredLanguagesChanged)) {
                *result = 13;
                return;
            }
        }
        {
            using _t = void (VlcQmlPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlPlayer::videoTrackChanged)) {
                *result = 14;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<VlcQmlPlayer *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->autoplay(); break;
        case 1: *reinterpret_cast< int*>(_v) = _t->deinterlacing(); break;
        case 2: *reinterpret_cast< qint64*>(_v) = _t->length(); break;
        case 3: *reinterpret_cast< int*>(_v) = _t->logLevel(); break;
        case 4: *reinterpret_cast< float*>(_v) = _t->position(); break;
        case 5: *reinterpret_cast< bool*>(_v) = _t->seekable(); break;
        case 6: *reinterpret_cast< int*>(_v) = _t->state(); break;
        case 7: *reinterpret_cast< qint64*>(_v) = _t->time(); break;
        case 8: *reinterpret_cast< QUrl*>(_v) = _t->url(); break;
        case 9: *reinterpret_cast< int*>(_v) = _t->volume(); break;
        case 10: *reinterpret_cast< VlcTrackModel**>(_v) = _t->audioTrackModel(); break;
        case 11: *reinterpret_cast< int*>(_v) = _t->audioTrack(); break;
        case 12: *reinterpret_cast< QStringList*>(_v) = _t->audioPreferredLanguages(); break;
        case 13: *reinterpret_cast< VlcTrackModel**>(_v) = _t->subtitleTrackModel(); break;
        case 14: *reinterpret_cast< int*>(_v) = _t->subtitleTrack(); break;
        case 15: *reinterpret_cast< QStringList*>(_v) = _t->subtitlePreferredLanguages(); break;
        case 16: *reinterpret_cast< VlcTrackModel**>(_v) = _t->videoTrackModel(); break;
        case 17: *reinterpret_cast< int*>(_v) = _t->videoTrack(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<VlcQmlPlayer *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setAutoplay(*reinterpret_cast< bool*>(_v)); break;
        case 1: _t->setDeinterlacing(*reinterpret_cast< int*>(_v)); break;
        case 3: _t->setLogLevel(*reinterpret_cast< int*>(_v)); break;
        case 4: _t->setPosition(*reinterpret_cast< float*>(_v)); break;
        case 7: _t->setTime(*reinterpret_cast< qint64*>(_v)); break;
        case 8: _t->setUrl(*reinterpret_cast< QUrl*>(_v)); break;
        case 9: _t->setVolume(*reinterpret_cast< int*>(_v)); break;
        case 11: _t->setAudioTrack(*reinterpret_cast< int*>(_v)); break;
        case 12: _t->setAudioPreferredLanguages(*reinterpret_cast< QStringList*>(_v)); break;
        case 14: _t->setSubtitleTrack(*reinterpret_cast< int*>(_v)); break;
        case 15: _t->setSubtitlePreferredLanguages(*reinterpret_cast< QStringList*>(_v)); break;
        case 17: _t->setVideoTrack(*reinterpret_cast< int*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject VlcQmlPlayer::staticMetaObject = { {
    QMetaObject::SuperData::link<VlcQmlSource::staticMetaObject>(),
    qt_meta_stringdata_VlcQmlPlayer.data,
    qt_meta_data_VlcQmlPlayer,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *VlcQmlPlayer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *VlcQmlPlayer::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_VlcQmlPlayer.stringdata0))
        return static_cast<void*>(this);
    return VlcQmlSource::qt_metacast(_clname);
}

int VlcQmlPlayer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = VlcQmlSource::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 20)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 20;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 20)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 20;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 18;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 18;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 18;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 18;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 18;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 18;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void VlcQmlPlayer::autoplayChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void VlcQmlPlayer::deinterlacingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void VlcQmlPlayer::lengthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void VlcQmlPlayer::logLevelChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void VlcQmlPlayer::positionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void VlcQmlPlayer::seekableChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void VlcQmlPlayer::stateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void VlcQmlPlayer::timeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void VlcQmlPlayer::urlChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void VlcQmlPlayer::volumeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}

// SIGNAL 10
void VlcQmlPlayer::audioTrackChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}

// SIGNAL 11
void VlcQmlPlayer::audioPreferredLanguagesChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, nullptr);
}

// SIGNAL 12
void VlcQmlPlayer::subtitleTrackChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, nullptr);
}

// SIGNAL 13
void VlcQmlPlayer::subtitlePreferredLanguagesChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 13, nullptr);
}

// SIGNAL 14
void VlcQmlPlayer::videoTrackChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 14, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
