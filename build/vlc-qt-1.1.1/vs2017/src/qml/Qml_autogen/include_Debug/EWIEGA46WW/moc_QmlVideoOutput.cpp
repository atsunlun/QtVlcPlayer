/****************************************************************************
** Meta object code from reading C++ file 'QmlVideoOutput.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../../../../src/qml/QmlVideoOutput.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QmlVideoOutput.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_VlcQmlVideoOutput_t {
    QByteArrayData data[14];
    char stringdata0[196];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_VlcQmlVideoOutput_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_VlcQmlVideoOutput_t qt_meta_stringdata_VlcQmlVideoOutput = {
    {
QT_MOC_LITERAL(0, 0, 17), // "VlcQmlVideoOutput"
QT_MOC_LITERAL(1, 18, 13), // "sourceChanged"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 15), // "fillModeChanged"
QT_MOC_LITERAL(4, 49, 18), // "aspectRatioChanged"
QT_MOC_LITERAL(5, 68, 16), // "cropRatioChanged"
QT_MOC_LITERAL(6, 85, 12), // "presentFrame"
QT_MOC_LITERAL(7, 98, 39), // "std::shared_ptr<const VlcYUVV..."
QT_MOC_LITERAL(8, 138, 5), // "frame"
QT_MOC_LITERAL(9, 144, 6), // "source"
QT_MOC_LITERAL(10, 151, 13), // "VlcQmlSource*"
QT_MOC_LITERAL(11, 165, 8), // "fillMode"
QT_MOC_LITERAL(12, 174, 11), // "aspectRatio"
QT_MOC_LITERAL(13, 186, 9) // "cropRatio"

    },
    "VlcQmlVideoOutput\0sourceChanged\0\0"
    "fillModeChanged\0aspectRatioChanged\0"
    "cropRatioChanged\0presentFrame\0"
    "std::shared_ptr<const VlcYUVVideoFrame>\0"
    "frame\0source\0VlcQmlSource*\0fillMode\0"
    "aspectRatio\0cropRatio"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_VlcQmlVideoOutput[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       4,   46, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   39,    2, 0x06 /* Public */,
       3,    0,   40,    2, 0x06 /* Public */,
       4,    0,   41,    2, 0x06 /* Public */,
       5,    0,   42,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    1,   43,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 7,    8,

 // properties: name, type, flags
       9, 0x80000000 | 10, 0x0049510b,
      11, QMetaType::Int, 0x00495103,
      12, QMetaType::Int, 0x00495103,
      13, QMetaType::Int, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,

       0        // eod
};

void VlcQmlVideoOutput::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<VlcQmlVideoOutput *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sourceChanged(); break;
        case 1: _t->fillModeChanged(); break;
        case 2: _t->aspectRatioChanged(); break;
        case 3: _t->cropRatioChanged(); break;
        case 4: _t->presentFrame((*reinterpret_cast< const std::shared_ptr<const VlcYUVVideoFrame>(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (VlcQmlVideoOutput::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlVideoOutput::sourceChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (VlcQmlVideoOutput::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlVideoOutput::fillModeChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (VlcQmlVideoOutput::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlVideoOutput::aspectRatioChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (VlcQmlVideoOutput::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlVideoOutput::cropRatioChanged)) {
                *result = 3;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<VlcQmlVideoOutput *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< VlcQmlSource**>(_v) = _t->source(); break;
        case 1: *reinterpret_cast< int*>(_v) = _t->fillMode(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->aspectRatio(); break;
        case 3: *reinterpret_cast< int*>(_v) = _t->cropRatio(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<VlcQmlVideoOutput *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setSource(*reinterpret_cast< VlcQmlSource**>(_v)); break;
        case 1: _t->setFillMode(*reinterpret_cast< int*>(_v)); break;
        case 2: _t->setAspectRatio(*reinterpret_cast< int*>(_v)); break;
        case 3: _t->setCropRatio(*reinterpret_cast< int*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject VlcQmlVideoOutput::staticMetaObject = { {
    QMetaObject::SuperData::link<QQuickItem::staticMetaObject>(),
    qt_meta_stringdata_VlcQmlVideoOutput.data,
    qt_meta_data_VlcQmlVideoOutput,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *VlcQmlVideoOutput::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *VlcQmlVideoOutput::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_VlcQmlVideoOutput.stringdata0))
        return static_cast<void*>(this);
    return QQuickItem::qt_metacast(_clname);
}

int VlcQmlVideoOutput::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 4;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void VlcQmlVideoOutput::sourceChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void VlcQmlVideoOutput::fillModeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void VlcQmlVideoOutput::aspectRatioChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void VlcQmlVideoOutput::cropRatioChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
