/****************************************************************************
** Meta object code from reading C++ file 'QmlVideoPlayer.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../../../../src/qml/QmlVideoPlayer.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QmlVideoPlayer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_VlcQmlVideoPlayer_t {
    QByteArrayData data[42];
    char stringdata0[595];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_VlcQmlVideoPlayer_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_VlcQmlVideoPlayer_t qt_meta_stringdata_VlcQmlVideoPlayer = {
    {
QT_MOC_LITERAL(0, 0, 17), // "VlcQmlVideoPlayer"
QT_MOC_LITERAL(1, 18, 13), // "volumeChanged"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 18), // "aspectRatioChanged"
QT_MOC_LITERAL(4, 52, 16), // "cropRatioChanged"
QT_MOC_LITERAL(5, 69, 20), // "deinterlacingChanged"
QT_MOC_LITERAL(6, 90, 12), // "stateChanged"
QT_MOC_LITERAL(7, 103, 15), // "seekableChanged"
QT_MOC_LITERAL(8, 119, 13), // "lengthChanged"
QT_MOC_LITERAL(9, 133, 11), // "timeChanged"
QT_MOC_LITERAL(10, 145, 15), // "positionChanged"
QT_MOC_LITERAL(11, 161, 17), // "audioTrackChanged"
QT_MOC_LITERAL(12, 179, 30), // "audioPreferredLanguagesChanged"
QT_MOC_LITERAL(13, 210, 20), // "subtitleTrackChanged"
QT_MOC_LITERAL(14, 231, 33), // "subtitlePreferredLanguagesCha..."
QT_MOC_LITERAL(15, 265, 17), // "videoTrackChanged"
QT_MOC_LITERAL(16, 283, 22), // "seekableChangedPrivate"
QT_MOC_LITERAL(17, 306, 11), // "mediaParsed"
QT_MOC_LITERAL(18, 318, 15), // "mediaPlayerVout"
QT_MOC_LITERAL(19, 334, 5), // "pause"
QT_MOC_LITERAL(20, 340, 4), // "play"
QT_MOC_LITERAL(21, 345, 4), // "stop"
QT_MOC_LITERAL(22, 350, 6), // "volume"
QT_MOC_LITERAL(23, 357, 11), // "aspectRatio"
QT_MOC_LITERAL(24, 369, 9), // "cropRatio"
QT_MOC_LITERAL(25, 379, 13), // "deinterlacing"
QT_MOC_LITERAL(26, 393, 3), // "url"
QT_MOC_LITERAL(27, 397, 8), // "autoplay"
QT_MOC_LITERAL(28, 406, 5), // "state"
QT_MOC_LITERAL(29, 412, 8), // "seekable"
QT_MOC_LITERAL(30, 421, 6), // "length"
QT_MOC_LITERAL(31, 428, 4), // "time"
QT_MOC_LITERAL(32, 433, 8), // "position"
QT_MOC_LITERAL(33, 442, 10), // "audioTrack"
QT_MOC_LITERAL(34, 453, 15), // "audioTrackModel"
QT_MOC_LITERAL(35, 469, 14), // "VlcTrackModel*"
QT_MOC_LITERAL(36, 484, 23), // "audioPreferredLanguages"
QT_MOC_LITERAL(37, 508, 13), // "subtitleTrack"
QT_MOC_LITERAL(38, 522, 18), // "subtitleTrackModel"
QT_MOC_LITERAL(39, 541, 26), // "subtitlePreferredLanguages"
QT_MOC_LITERAL(40, 568, 10), // "videoTrack"
QT_MOC_LITERAL(41, 579, 15) // "videoTrackModel"

    },
    "VlcQmlVideoPlayer\0volumeChanged\0\0"
    "aspectRatioChanged\0cropRatioChanged\0"
    "deinterlacingChanged\0stateChanged\0"
    "seekableChanged\0lengthChanged\0timeChanged\0"
    "positionChanged\0audioTrackChanged\0"
    "audioPreferredLanguagesChanged\0"
    "subtitleTrackChanged\0"
    "subtitlePreferredLanguagesChanged\0"
    "videoTrackChanged\0seekableChangedPrivate\0"
    "mediaParsed\0mediaPlayerVout\0pause\0"
    "play\0stop\0volume\0aspectRatio\0cropRatio\0"
    "deinterlacing\0url\0autoplay\0state\0"
    "seekable\0length\0time\0position\0audioTrack\0"
    "audioTrackModel\0VlcTrackModel*\0"
    "audioPreferredLanguages\0subtitleTrack\0"
    "subtitleTrackModel\0subtitlePreferredLanguages\0"
    "videoTrack\0videoTrackModel"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_VlcQmlVideoPlayer[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      20,   14, // methods
      19,  140, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      14,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  114,    2, 0x06 /* Public */,
       3,    0,  115,    2, 0x06 /* Public */,
       4,    0,  116,    2, 0x06 /* Public */,
       5,    0,  117,    2, 0x06 /* Public */,
       6,    0,  118,    2, 0x06 /* Public */,
       7,    0,  119,    2, 0x06 /* Public */,
       8,    0,  120,    2, 0x06 /* Public */,
       9,    0,  121,    2, 0x06 /* Public */,
      10,    0,  122,    2, 0x06 /* Public */,
      11,    0,  123,    2, 0x06 /* Public */,
      12,    0,  124,    2, 0x06 /* Public */,
      13,    0,  125,    2, 0x06 /* Public */,
      14,    0,  126,    2, 0x06 /* Public */,
      15,    0,  127,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      16,    1,  128,    2, 0x08 /* Private */,
      17,    1,  131,    2, 0x08 /* Private */,
      18,    1,  134,    2, 0x08 /* Private */,

 // methods: name, argc, parameters, tag, flags
      19,    0,  137,    2, 0x02 /* Public */,
      20,    0,  138,    2, 0x02 /* Public */,
      21,    0,  139,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Int,    2,

 // methods: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      22, QMetaType::Int, 0x00495103,
      23, QMetaType::QString, 0x00495103,
      24, QMetaType::QString, 0x00495103,
      25, QMetaType::QString, 0x00495103,
      26, QMetaType::QUrl, 0x00095103,
      27, QMetaType::Bool, 0x00095103,
      28, QMetaType::Int, 0x00495001,
      29, QMetaType::Bool, 0x00495001,
      30, QMetaType::Int, 0x00495001,
      31, QMetaType::Int, 0x00495103,
      32, QMetaType::Float, 0x00495103,
      33, QMetaType::Int, 0x00495103,
      34, 0x80000000 | 35, 0x00095409,
      36, QMetaType::QStringList, 0x00495103,
      37, QMetaType::Int, 0x00495103,
      38, 0x80000000 | 35, 0x00095409,
      39, QMetaType::QStringList, 0x00495103,
      40, QMetaType::Int, 0x00495103,
      41, 0x80000000 | 35, 0x00095409,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       0,
       0,
       4,
       5,
       6,
       7,
       8,
       9,
       0,
      10,
      11,
       0,
      12,
      13,
       0,

       0        // eod
};

void VlcQmlVideoPlayer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<VlcQmlVideoPlayer *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->volumeChanged(); break;
        case 1: _t->aspectRatioChanged(); break;
        case 2: _t->cropRatioChanged(); break;
        case 3: _t->deinterlacingChanged(); break;
        case 4: _t->stateChanged(); break;
        case 5: _t->seekableChanged(); break;
        case 6: _t->lengthChanged(); break;
        case 7: _t->timeChanged(); break;
        case 8: _t->positionChanged(); break;
        case 9: _t->audioTrackChanged(); break;
        case 10: _t->audioPreferredLanguagesChanged(); break;
        case 11: _t->subtitleTrackChanged(); break;
        case 12: _t->subtitlePreferredLanguagesChanged(); break;
        case 13: _t->videoTrackChanged(); break;
        case 14: _t->seekableChangedPrivate((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 15: _t->mediaParsed((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 16: _t->mediaPlayerVout((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->pause(); break;
        case 18: _t->play(); break;
        case 19: _t->stop(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (VlcQmlVideoPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlVideoPlayer::volumeChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (VlcQmlVideoPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlVideoPlayer::aspectRatioChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (VlcQmlVideoPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlVideoPlayer::cropRatioChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (VlcQmlVideoPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlVideoPlayer::deinterlacingChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (VlcQmlVideoPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlVideoPlayer::stateChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (VlcQmlVideoPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlVideoPlayer::seekableChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (VlcQmlVideoPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlVideoPlayer::lengthChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (VlcQmlVideoPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlVideoPlayer::timeChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (VlcQmlVideoPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlVideoPlayer::positionChanged)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (VlcQmlVideoPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlVideoPlayer::audioTrackChanged)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (VlcQmlVideoPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlVideoPlayer::audioPreferredLanguagesChanged)) {
                *result = 10;
                return;
            }
        }
        {
            using _t = void (VlcQmlVideoPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlVideoPlayer::subtitleTrackChanged)) {
                *result = 11;
                return;
            }
        }
        {
            using _t = void (VlcQmlVideoPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlVideoPlayer::subtitlePreferredLanguagesChanged)) {
                *result = 12;
                return;
            }
        }
        {
            using _t = void (VlcQmlVideoPlayer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&VlcQmlVideoPlayer::videoTrackChanged)) {
                *result = 13;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 18:
        case 15:
        case 12:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< VlcTrackModel* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<VlcQmlVideoPlayer *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = _t->volume(); break;
        case 1: *reinterpret_cast< QString*>(_v) = _t->aspectRatio(); break;
        case 2: *reinterpret_cast< QString*>(_v) = _t->cropRatio(); break;
        case 3: *reinterpret_cast< QString*>(_v) = _t->deinterlacing(); break;
        case 4: *reinterpret_cast< QUrl*>(_v) = _t->url(); break;
        case 5: *reinterpret_cast< bool*>(_v) = _t->autoplay(); break;
        case 6: *reinterpret_cast< int*>(_v) = _t->state(); break;
        case 7: *reinterpret_cast< bool*>(_v) = _t->seekable(); break;
        case 8: *reinterpret_cast< int*>(_v) = _t->length(); break;
        case 9: *reinterpret_cast< int*>(_v) = _t->time(); break;
        case 10: *reinterpret_cast< float*>(_v) = _t->position(); break;
        case 11: *reinterpret_cast< int*>(_v) = _t->audioTrack(); break;
        case 12: *reinterpret_cast< VlcTrackModel**>(_v) = _t->audioTrackModel(); break;
        case 13: *reinterpret_cast< QStringList*>(_v) = _t->audioPreferredLanguages(); break;
        case 14: *reinterpret_cast< int*>(_v) = _t->subtitleTrack(); break;
        case 15: *reinterpret_cast< VlcTrackModel**>(_v) = _t->subtitleTrackModel(); break;
        case 16: *reinterpret_cast< QStringList*>(_v) = _t->subtitlePreferredLanguages(); break;
        case 17: *reinterpret_cast< int*>(_v) = _t->videoTrack(); break;
        case 18: *reinterpret_cast< VlcTrackModel**>(_v) = _t->videoTrackModel(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<VlcQmlVideoPlayer *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setVolume(*reinterpret_cast< int*>(_v)); break;
        case 1: _t->setAspectRatio(*reinterpret_cast< QString*>(_v)); break;
        case 2: _t->setCropRatio(*reinterpret_cast< QString*>(_v)); break;
        case 3: _t->setDeinterlacing(*reinterpret_cast< QString*>(_v)); break;
        case 4: _t->setUrl(*reinterpret_cast< QUrl*>(_v)); break;
        case 5: _t->setAutoplay(*reinterpret_cast< bool*>(_v)); break;
        case 9: _t->setTime(*reinterpret_cast< int*>(_v)); break;
        case 10: _t->setPosition(*reinterpret_cast< float*>(_v)); break;
        case 11: _t->setAudioTrack(*reinterpret_cast< int*>(_v)); break;
        case 13: _t->setAudioPreferredLanguages(*reinterpret_cast< QStringList*>(_v)); break;
        case 14: _t->setSubtitleTrack(*reinterpret_cast< int*>(_v)); break;
        case 16: _t->setSubtitlePreferredLanguages(*reinterpret_cast< QStringList*>(_v)); break;
        case 17: _t->setVideoTrack(*reinterpret_cast< int*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject VlcQmlVideoPlayer::staticMetaObject = { {
    QMetaObject::SuperData::link<VlcQmlVideoObject::staticMetaObject>(),
    qt_meta_stringdata_VlcQmlVideoPlayer.data,
    qt_meta_data_VlcQmlVideoPlayer,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *VlcQmlVideoPlayer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *VlcQmlVideoPlayer::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_VlcQmlVideoPlayer.stringdata0))
        return static_cast<void*>(this);
    return VlcQmlVideoObject::qt_metacast(_clname);
}

int VlcQmlVideoPlayer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = VlcQmlVideoObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 20)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 20;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 20)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 20;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 19;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 19;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 19;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 19;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 19;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void VlcQmlVideoPlayer::volumeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void VlcQmlVideoPlayer::aspectRatioChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void VlcQmlVideoPlayer::cropRatioChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void VlcQmlVideoPlayer::deinterlacingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void VlcQmlVideoPlayer::stateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void VlcQmlVideoPlayer::seekableChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void VlcQmlVideoPlayer::lengthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void VlcQmlVideoPlayer::timeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void VlcQmlVideoPlayer::positionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void VlcQmlVideoPlayer::audioTrackChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}

// SIGNAL 10
void VlcQmlVideoPlayer::audioPreferredLanguagesChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}

// SIGNAL 11
void VlcQmlVideoPlayer::subtitleTrackChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, nullptr);
}

// SIGNAL 12
void VlcQmlVideoPlayer::subtitlePreferredLanguagesChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, nullptr);
}

// SIGNAL 13
void VlcQmlVideoPlayer::videoTrackChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 13, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
