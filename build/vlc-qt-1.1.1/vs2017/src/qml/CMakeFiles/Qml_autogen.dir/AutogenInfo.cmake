# Meta
set(AM_MULTI_CONFIG "TRUE")
set(AM_PARALLEL "6")
set(AM_VERBOSITY "")
# Directories
set(AM_CMAKE_SOURCE_DIR "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1")
set(AM_CMAKE_BINARY_DIR "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017")
set(AM_CMAKE_CURRENT_SOURCE_DIR "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml")
set(AM_CMAKE_CURRENT_BINARY_DIR "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/qml")
set(AM_CMAKE_INCLUDE_DIRECTORIES_PROJECT_BEFORE "")
set(AM_BUILD_DIR "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/qml/Qml_autogen")
set(AM_INCLUDE_DIR "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/qml/Qml_autogen/include_\$<CONFIG>")
set(AM_INCLUDE_DIR_Debug "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/qml/Qml_autogen/include_Debug")
set(AM_INCLUDE_DIR_MinSizeRel "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/qml/Qml_autogen/include_MinSizeRel")
set(AM_INCLUDE_DIR_RelWithDebInfo "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/qml/Qml_autogen/include_RelWithDebInfo")
set(AM_INCLUDE_DIR_Release "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/qml/Qml_autogen/include_Release")
# Qt
set(AM_QT_VERSION_MAJOR 5)
set(AM_QT_MOC_EXECUTABLE "D:/Qt/Qt5.14.2/5.14.2/msvc2017/bin/moc.exe")
set(AM_QT_UIC_EXECUTABLE "D:/Qt/Qt5.14.2/5.14.2/msvc2017/bin/uic.exe")
# Files
set(AM_CMAKE_EXECUTABLE "D:/Program Files/CMake/bin/cmake.exe")
set(AM_SETTINGS_FILE "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/qml/CMakeFiles/Qml_autogen.dir/AutogenOldSettings.txt")
set(AM_SETTINGS_FILE_Debug "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/qml/CMakeFiles/Qml_autogen.dir/AutogenOldSettings_Debug.txt")
set(AM_SETTINGS_FILE_MinSizeRel "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/qml/CMakeFiles/Qml_autogen.dir/AutogenOldSettings_MinSizeRel.txt")
set(AM_SETTINGS_FILE_RelWithDebInfo "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/qml/CMakeFiles/Qml_autogen.dir/AutogenOldSettings_RelWithDebInfo.txt")
set(AM_SETTINGS_FILE_Release "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/qml/CMakeFiles/Qml_autogen.dir/AutogenOldSettings_Release.txt")
set(AM_PARSE_CACHE_FILE "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/qml/CMakeFiles/Qml_autogen.dir/ParseCache.txt")
set(AM_HEADERS "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml/Qml.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml/QmlPlayer.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml/QmlSource.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml/QmlVideoObject.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml/QmlVideoOutput.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml/QmlVideoPlayer.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml/SharedExportQml.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml/painter/GlPainter.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml/painter/GlslPainter.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml/rendering/QmlVideoStream.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml/rendering/VideoMaterial.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml/rendering/VideoMaterialShader.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml/rendering/VideoNode.h")
set(AM_HEADERS_FLAGS "MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU")
set(AM_HEADERS_BUILD_PATHS "EWIEGA46WW/moc_Qml.cpp;EWIEGA46WW/moc_QmlPlayer.cpp;EWIEGA46WW/moc_QmlSource.cpp;EWIEGA46WW/moc_QmlVideoObject.cpp;EWIEGA46WW/moc_QmlVideoOutput.cpp;EWIEGA46WW/moc_QmlVideoPlayer.cpp;EWIEGA46WW/moc_SharedExportQml.cpp;LOQAMH6MBH/moc_GlPainter.cpp;LOQAMH6MBH/moc_GlslPainter.cpp;LHXIM2V6NU/moc_QmlVideoStream.cpp;LHXIM2V6NU/moc_VideoMaterial.cpp;LHXIM2V6NU/moc_VideoMaterialShader.cpp;LHXIM2V6NU/moc_VideoNode.cpp")
set(AM_SOURCES "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml/Qml.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml/QmlPlayer.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml/QmlSource.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml/QmlVideoObject.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml/QmlVideoOutput.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml/QmlVideoPlayer.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml/painter/GlPainter.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml/painter/GlslPainter.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml/rendering/QmlVideoStream.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml/rendering/VideoMaterial.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml/rendering/VideoMaterialShader.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml/rendering/VideoNode.cpp")
set(AM_SOURCES_FLAGS "MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU")
# MOC settings
set(AM_MOC_SKIP "")
set(AM_MOC_DEFINITIONS "NOMINMAX;QT_CORE_LIB;QT_GUI_LIB;QT_NETWORK_LIB;QT_NO_DEBUG;QT_QMLMODELS_LIB;QT_QML_LIB;QT_QUICK_LIB;QT_SHARED;Qml_EXPORTS;VLCQT_QML_LIBRARY;WIN32;_REENTRANT")
set(AM_MOC_DEFINITIONS_Debug "NOMINMAX;QT_CORE_LIB;QT_GUI_LIB;QT_NETWORK_LIB;QT_QMLMODELS_LIB;QT_QML_LIB;QT_QUICK_LIB;QT_SHARED;Qml_EXPORTS;VLCQT_QML_LIBRARY;WIN32;_REENTRANT")
set(AM_MOC_INCLUDES "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/qml;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/include;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/libvlc-headers/include;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/libvlc-headers/include/vlc/plugins;D:/Qt/Qt5.14.2/5.14.2/msvc2017/include/vlc/plugins;D:/Qt/Qt5.14.2/5.14.2/msvc2017/include;D:/Qt/Qt5.14.2/5.14.2/msvc2017/include/QtQuick;D:/Qt/Qt5.14.2/5.14.2/msvc2017/include/QtQmlModels;D:/Qt/Qt5.14.2/5.14.2/msvc2017/include/QtQml;D:/Qt/Qt5.14.2/5.14.2/msvc2017/include/QtNetwork;D:/Qt/Qt5.14.2/5.14.2/msvc2017/include/QtCore;D:/Qt/Qt5.14.2/5.14.2/msvc2017/./mkspecs/win32-msvc;D:/Qt/Qt5.14.2/5.14.2/msvc2017/include/QtGui;D:/Qt/Qt5.14.2/5.14.2/msvc2017/include/QtANGLE")
set(AM_MOC_OPTIONS "")
set(AM_MOC_RELAXED_MODE "")
set(AM_MOC_MACRO_NAMES "Q_OBJECT;Q_GADGET;Q_NAMESPACE;Q_NAMESPACE_EXPORT")
set(AM_MOC_DEPEND_FILTERS "")
set(AM_MOC_PREDEFS_CMD "")
# UIC settings
set(AM_UIC_SKIP "")
set(AM_UIC_TARGET_OPTIONS "")
set(AM_UIC_OPTIONS_FILES "")
set(AM_UIC_OPTIONS_OPTIONS "")
set(AM_UIC_SEARCH_PATHS "")
