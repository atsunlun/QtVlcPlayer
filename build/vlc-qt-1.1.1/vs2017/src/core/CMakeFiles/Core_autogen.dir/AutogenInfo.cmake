# Meta
set(AM_MULTI_CONFIG "TRUE")
set(AM_PARALLEL "6")
set(AM_VERBOSITY "")
# Directories
set(AM_CMAKE_SOURCE_DIR "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1")
set(AM_CMAKE_BINARY_DIR "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017")
set(AM_CMAKE_CURRENT_SOURCE_DIR "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core")
set(AM_CMAKE_CURRENT_BINARY_DIR "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/core")
set(AM_CMAKE_INCLUDE_DIRECTORIES_PROJECT_BEFORE "")
set(AM_BUILD_DIR "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/core/Core_autogen")
set(AM_INCLUDE_DIR "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/core/Core_autogen/include_\$<CONFIG>")
set(AM_INCLUDE_DIR_Debug "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/core/Core_autogen/include_Debug")
set(AM_INCLUDE_DIR_MinSizeRel "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/core/Core_autogen/include_MinSizeRel")
set(AM_INCLUDE_DIR_RelWithDebInfo "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/core/Core_autogen/include_RelWithDebInfo")
set(AM_INCLUDE_DIR_Release "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/core/Core_autogen/include_Release")
# Qt
set(AM_QT_VERSION_MAJOR 5)
set(AM_QT_MOC_EXECUTABLE "D:/Qt/Qt5.14.2/5.14.2/msvc2017/bin/moc.exe")
set(AM_QT_UIC_EXECUTABLE "D:/Qt/Qt5.14.2/5.14.2/msvc2017/bin/uic.exe")
# Files
set(AM_CMAKE_EXECUTABLE "D:/Program Files/CMake/bin/cmake.exe")
set(AM_SETTINGS_FILE "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/core/CMakeFiles/Core_autogen.dir/AutogenOldSettings.txt")
set(AM_SETTINGS_FILE_Debug "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/core/CMakeFiles/Core_autogen.dir/AutogenOldSettings_Debug.txt")
set(AM_SETTINGS_FILE_MinSizeRel "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/core/CMakeFiles/Core_autogen.dir/AutogenOldSettings_MinSizeRel.txt")
set(AM_SETTINGS_FILE_RelWithDebInfo "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/core/CMakeFiles/Core_autogen.dir/AutogenOldSettings_RelWithDebInfo.txt")
set(AM_SETTINGS_FILE_Release "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/core/CMakeFiles/Core_autogen.dir/AutogenOldSettings_Release.txt")
set(AM_PARSE_CACHE_FILE "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/core/CMakeFiles/Core_autogen.dir/ParseCache.txt")
set(AM_HEADERS "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/AbstractVideoFrame.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/AbstractVideoStream.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/Audio.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/Common.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/Enums.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/Equalizer.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/Error.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/Instance.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/Media.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/MediaList.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/MediaListPlayer.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/MediaPlayer.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/MetaManager.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/ModuleDescription.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/SharedExportCore.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/Stats.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/TrackModel.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/Video.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/VideoDelegate.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/VideoFrame.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/VideoMemoryStream.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/VideoStream.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/YUVVideoFrame.h;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src/Config.h")
set(AM_HEADERS_FLAGS "MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU")
set(AM_HEADERS_BUILD_PATHS "EWIEGA46WW/moc_AbstractVideoFrame.cpp;EWIEGA46WW/moc_AbstractVideoStream.cpp;EWIEGA46WW/moc_Audio.cpp;EWIEGA46WW/moc_Common.cpp;EWIEGA46WW/moc_Enums.cpp;EWIEGA46WW/moc_Equalizer.cpp;EWIEGA46WW/moc_Error.cpp;EWIEGA46WW/moc_Instance.cpp;EWIEGA46WW/moc_Media.cpp;EWIEGA46WW/moc_MediaList.cpp;EWIEGA46WW/moc_MediaListPlayer.cpp;EWIEGA46WW/moc_MediaPlayer.cpp;EWIEGA46WW/moc_MetaManager.cpp;EWIEGA46WW/moc_ModuleDescription.cpp;EWIEGA46WW/moc_SharedExportCore.cpp;EWIEGA46WW/moc_Stats.cpp;EWIEGA46WW/moc_TrackModel.cpp;EWIEGA46WW/moc_Video.cpp;EWIEGA46WW/moc_VideoDelegate.cpp;EWIEGA46WW/moc_VideoFrame.cpp;EWIEGA46WW/moc_VideoMemoryStream.cpp;EWIEGA46WW/moc_VideoStream.cpp;EWIEGA46WW/moc_YUVVideoFrame.cpp;MC4TX37JBE/moc_Config.cpp")
set(AM_SOURCES "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/AbstractVideoFrame.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/AbstractVideoStream.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/Audio.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/Common.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/Enums.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/Equalizer.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/Error.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/Instance.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/Media.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/MediaList.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/MediaListPlayer.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/MediaPlayer.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/MetaManager.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/ModuleDescription.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/TrackModel.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/Video.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/VideoFrame.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/VideoMemoryStream.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/VideoStream.cpp;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core/YUVVideoFrame.cpp")
set(AM_SOURCES_FLAGS "MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU;MU")
# MOC settings
set(AM_MOC_SKIP "")
set(AM_MOC_DEFINITIONS "Core_EXPORTS;QT_CORE_LIB;QT_NO_DEBUG;QT_SHARED;VLCQT_CORE_LIBRARY;WIN32;_REENTRANT")
set(AM_MOC_DEFINITIONS_Debug "Core_EXPORTS;QT_CORE_LIB;QT_SHARED;VLCQT_CORE_LIBRARY;WIN32;_REENTRANT")
set(AM_MOC_INCLUDES "E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/libvlc-headers/include;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/libvlc-headers/include/vlc/plugins;D:/Qt/Qt5.14.2/5.14.2/msvc2017/include/vlc/plugins;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/src/core;E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/vs2017/src;D:/Qt/Qt5.14.2/5.14.2/msvc2017/include;D:/Qt/Qt5.14.2/5.14.2/msvc2017/include/QtCore;D:/Qt/Qt5.14.2/5.14.2/msvc2017/./mkspecs/win32-msvc")
set(AM_MOC_OPTIONS "")
set(AM_MOC_RELAXED_MODE "")
set(AM_MOC_MACRO_NAMES "Q_OBJECT;Q_GADGET;Q_NAMESPACE;Q_NAMESPACE_EXPORT")
set(AM_MOC_DEPEND_FILTERS "")
set(AM_MOC_PREDEFS_CMD "")
# UIC settings
set(AM_UIC_SKIP "")
set(AM_UIC_TARGET_OPTIONS "")
set(AM_UIC_OPTIONS_FILES "")
set(AM_UIC_OPTIONS_OPTIONS "")
set(AM_UIC_SEARCH_PATHS "")
