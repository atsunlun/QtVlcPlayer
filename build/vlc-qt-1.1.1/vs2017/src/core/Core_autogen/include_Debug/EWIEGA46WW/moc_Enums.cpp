/****************************************************************************
** Meta object code from reading C++ file 'Enums.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../../../../src/core/Enums.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Enums.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Vlc_t {
    QByteArrayData data[118];
    char stringdata0[962];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Vlc_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Vlc_t qt_meta_stringdata_Vlc = {
    {
QT_MOC_LITERAL(0, 0, 3), // "Vlc"
QT_MOC_LITERAL(1, 4, 8), // "LogLevel"
QT_MOC_LITERAL(2, 13, 10), // "DebugLevel"
QT_MOC_LITERAL(3, 24, 11), // "NoticeLevel"
QT_MOC_LITERAL(4, 36, 12), // "WarningLevel"
QT_MOC_LITERAL(5, 49, 10), // "ErrorLevel"
QT_MOC_LITERAL(6, 60, 13), // "DisabledLevel"
QT_MOC_LITERAL(7, 74, 11), // "ActionsType"
QT_MOC_LITERAL(8, 86, 10), // "AudioTrack"
QT_MOC_LITERAL(9, 97, 9), // "Subtitles"
QT_MOC_LITERAL(10, 107, 10), // "VideoTrack"
QT_MOC_LITERAL(11, 118, 5), // "Other"
QT_MOC_LITERAL(12, 124, 12), // "AudioChannel"
QT_MOC_LITERAL(13, 137, 17), // "AudioChannelError"
QT_MOC_LITERAL(14, 155, 6), // "Stereo"
QT_MOC_LITERAL(15, 162, 7), // "RStereo"
QT_MOC_LITERAL(16, 170, 4), // "Left"
QT_MOC_LITERAL(17, 175, 5), // "Right"
QT_MOC_LITERAL(18, 181, 6), // "Dolbys"
QT_MOC_LITERAL(19, 188, 10), // "AudioCodec"
QT_MOC_LITERAL(20, 199, 7), // "NoAudio"
QT_MOC_LITERAL(21, 207, 10), // "MPEG2Audio"
QT_MOC_LITERAL(22, 218, 3), // "MP3"
QT_MOC_LITERAL(23, 222, 10), // "MPEG4Audio"
QT_MOC_LITERAL(24, 233, 6), // "Vorbis"
QT_MOC_LITERAL(25, 240, 4), // "Flac"
QT_MOC_LITERAL(26, 245, 11), // "AudioOutput"
QT_MOC_LITERAL(27, 257, 11), // "DefaultAout"
QT_MOC_LITERAL(28, 269, 13), // "Deinterlacing"
QT_MOC_LITERAL(29, 283, 8), // "Disabled"
QT_MOC_LITERAL(30, 292, 7), // "Discard"
QT_MOC_LITERAL(31, 300, 5), // "Blend"
QT_MOC_LITERAL(32, 306, 4), // "Mean"
QT_MOC_LITERAL(33, 311, 3), // "Bob"
QT_MOC_LITERAL(34, 315, 6), // "Linear"
QT_MOC_LITERAL(35, 322, 1), // "X"
QT_MOC_LITERAL(36, 324, 5), // "Yadif"
QT_MOC_LITERAL(37, 330, 7), // "Yadif2x"
QT_MOC_LITERAL(38, 338, 7), // "Phospor"
QT_MOC_LITERAL(39, 346, 4), // "IVTC"
QT_MOC_LITERAL(40, 351, 8), // "FillMode"
QT_MOC_LITERAL(41, 360, 17), // "PreserveAspectFit"
QT_MOC_LITERAL(42, 378, 18), // "PreserveAspectCrop"
QT_MOC_LITERAL(43, 397, 7), // "Stretch"
QT_MOC_LITERAL(44, 405, 4), // "Meta"
QT_MOC_LITERAL(45, 410, 5), // "Title"
QT_MOC_LITERAL(46, 416, 6), // "Artist"
QT_MOC_LITERAL(47, 423, 5), // "Genre"
QT_MOC_LITERAL(48, 429, 9), // "Copyright"
QT_MOC_LITERAL(49, 439, 5), // "Album"
QT_MOC_LITERAL(50, 445, 11), // "TrackNumber"
QT_MOC_LITERAL(51, 457, 11), // "Description"
QT_MOC_LITERAL(52, 469, 6), // "Rating"
QT_MOC_LITERAL(53, 476, 4), // "Date"
QT_MOC_LITERAL(54, 481, 7), // "Setting"
QT_MOC_LITERAL(55, 489, 3), // "URL"
QT_MOC_LITERAL(56, 493, 8), // "Language"
QT_MOC_LITERAL(57, 502, 10), // "NowPlaying"
QT_MOC_LITERAL(58, 513, 9), // "Publisher"
QT_MOC_LITERAL(59, 523, 9), // "EncodedBy"
QT_MOC_LITERAL(60, 533, 10), // "ArtworkURL"
QT_MOC_LITERAL(61, 544, 7), // "TrackID"
QT_MOC_LITERAL(62, 552, 3), // "Mux"
QT_MOC_LITERAL(63, 556, 2), // "TS"
QT_MOC_LITERAL(64, 559, 2), // "PS"
QT_MOC_LITERAL(65, 562, 3), // "MP4"
QT_MOC_LITERAL(66, 566, 3), // "OGG"
QT_MOC_LITERAL(67, 570, 3), // "AVI"
QT_MOC_LITERAL(68, 574, 12), // "PlaybackMode"
QT_MOC_LITERAL(69, 587, 15), // "DefaultPlayback"
QT_MOC_LITERAL(70, 603, 4), // "Loop"
QT_MOC_LITERAL(71, 608, 6), // "Repeat"
QT_MOC_LITERAL(72, 615, 5), // "Ratio"
QT_MOC_LITERAL(73, 621, 8), // "Original"
QT_MOC_LITERAL(74, 630, 6), // "Ignore"
QT_MOC_LITERAL(75, 637, 6), // "R_16_9"
QT_MOC_LITERAL(76, 644, 7), // "R_16_10"
QT_MOC_LITERAL(77, 652, 9), // "R_185_100"
QT_MOC_LITERAL(78, 662, 9), // "R_221_100"
QT_MOC_LITERAL(79, 672, 9), // "R_235_100"
QT_MOC_LITERAL(80, 682, 9), // "R_239_100"
QT_MOC_LITERAL(81, 692, 5), // "R_4_3"
QT_MOC_LITERAL(82, 698, 5), // "R_5_4"
QT_MOC_LITERAL(83, 704, 5), // "R_5_3"
QT_MOC_LITERAL(84, 710, 5), // "R_1_1"
QT_MOC_LITERAL(85, 716, 5), // "Scale"
QT_MOC_LITERAL(86, 722, 7), // "NoScale"
QT_MOC_LITERAL(87, 730, 6), // "S_1_05"
QT_MOC_LITERAL(88, 737, 5), // "S_1_1"
QT_MOC_LITERAL(89, 743, 5), // "S_1_2"
QT_MOC_LITERAL(90, 749, 5), // "S_1_3"
QT_MOC_LITERAL(91, 755, 5), // "S_1_4"
QT_MOC_LITERAL(92, 761, 5), // "S_1_5"
QT_MOC_LITERAL(93, 767, 5), // "S_1_6"
QT_MOC_LITERAL(94, 773, 5), // "S_1_7"
QT_MOC_LITERAL(95, 779, 5), // "S_1_8"
QT_MOC_LITERAL(96, 785, 5), // "S_1_9"
QT_MOC_LITERAL(97, 791, 5), // "S_2_0"
QT_MOC_LITERAL(98, 797, 5), // "State"
QT_MOC_LITERAL(99, 803, 4), // "Idle"
QT_MOC_LITERAL(100, 808, 7), // "Opening"
QT_MOC_LITERAL(101, 816, 9), // "Buffering"
QT_MOC_LITERAL(102, 826, 7), // "Playing"
QT_MOC_LITERAL(103, 834, 6), // "Paused"
QT_MOC_LITERAL(104, 841, 7), // "Stopped"
QT_MOC_LITERAL(105, 849, 5), // "Ended"
QT_MOC_LITERAL(106, 855, 5), // "Error"
QT_MOC_LITERAL(107, 861, 10), // "VideoCodec"
QT_MOC_LITERAL(108, 872, 7), // "NoVideo"
QT_MOC_LITERAL(109, 880, 10), // "MPEG2Video"
QT_MOC_LITERAL(110, 891, 10), // "MPEG4Video"
QT_MOC_LITERAL(111, 902, 4), // "H264"
QT_MOC_LITERAL(112, 907, 6), // "Theora"
QT_MOC_LITERAL(113, 914, 11), // "VideoOutput"
QT_MOC_LITERAL(114, 926, 7), // "DirectX"
QT_MOC_LITERAL(115, 934, 8), // "Direct3D"
QT_MOC_LITERAL(116, 943, 6), // "OpenGL"
QT_MOC_LITERAL(117, 950, 11) // "DefaultVout"

    },
    "Vlc\0LogLevel\0DebugLevel\0NoticeLevel\0"
    "WarningLevel\0ErrorLevel\0DisabledLevel\0"
    "ActionsType\0AudioTrack\0Subtitles\0"
    "VideoTrack\0Other\0AudioChannel\0"
    "AudioChannelError\0Stereo\0RStereo\0Left\0"
    "Right\0Dolbys\0AudioCodec\0NoAudio\0"
    "MPEG2Audio\0MP3\0MPEG4Audio\0Vorbis\0Flac\0"
    "AudioOutput\0DefaultAout\0Deinterlacing\0"
    "Disabled\0Discard\0Blend\0Mean\0Bob\0Linear\0"
    "X\0Yadif\0Yadif2x\0Phospor\0IVTC\0FillMode\0"
    "PreserveAspectFit\0PreserveAspectCrop\0"
    "Stretch\0Meta\0Title\0Artist\0Genre\0"
    "Copyright\0Album\0TrackNumber\0Description\0"
    "Rating\0Date\0Setting\0URL\0Language\0"
    "NowPlaying\0Publisher\0EncodedBy\0"
    "ArtworkURL\0TrackID\0Mux\0TS\0PS\0MP4\0OGG\0"
    "AVI\0PlaybackMode\0DefaultPlayback\0Loop\0"
    "Repeat\0Ratio\0Original\0Ignore\0R_16_9\0"
    "R_16_10\0R_185_100\0R_221_100\0R_235_100\0"
    "R_239_100\0R_4_3\0R_5_4\0R_5_3\0R_1_1\0"
    "Scale\0NoScale\0S_1_05\0S_1_1\0S_1_2\0S_1_3\0"
    "S_1_4\0S_1_5\0S_1_6\0S_1_7\0S_1_8\0S_1_9\0"
    "S_2_0\0State\0Idle\0Opening\0Buffering\0"
    "Playing\0Paused\0Stopped\0Ended\0Error\0"
    "VideoCodec\0NoVideo\0MPEG2Video\0MPEG4Video\0"
    "H264\0Theora\0VideoOutput\0DirectX\0"
    "Direct3D\0OpenGL\0DefaultVout"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Vlc[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
      15,   14, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // enums: name, alias, flags, count, data
       1,    1, 0x0,    5,   89,
       7,    7, 0x0,    4,   99,
      12,   12, 0x0,    6,  107,
      19,   19, 0x0,    6,  119,
      26,   26, 0x0,    1,  131,
      28,   28, 0x0,   11,  133,
      40,   40, 0x0,    3,  155,
      44,   44, 0x0,   17,  161,
      62,   62, 0x0,    5,  195,
      68,   68, 0x0,    3,  205,
      72,   72, 0x0,   12,  211,
      85,   85, 0x0,   12,  235,
      98,   98, 0x0,    8,  259,
     107,  107, 0x0,    5,  275,
     113,  113, 0x0,    4,  285,

 // enum data: key, value
       2, uint(Vlc::DebugLevel),
       3, uint(Vlc::NoticeLevel),
       4, uint(Vlc::WarningLevel),
       5, uint(Vlc::ErrorLevel),
       6, uint(Vlc::DisabledLevel),
       8, uint(Vlc::AudioTrack),
       9, uint(Vlc::Subtitles),
      10, uint(Vlc::VideoTrack),
      11, uint(Vlc::Other),
      13, uint(Vlc::AudioChannelError),
      14, uint(Vlc::Stereo),
      15, uint(Vlc::RStereo),
      16, uint(Vlc::Left),
      17, uint(Vlc::Right),
      18, uint(Vlc::Dolbys),
      20, uint(Vlc::NoAudio),
      21, uint(Vlc::MPEG2Audio),
      22, uint(Vlc::MP3),
      23, uint(Vlc::MPEG4Audio),
      24, uint(Vlc::Vorbis),
      25, uint(Vlc::Flac),
      27, uint(Vlc::DefaultAout),
      29, uint(Vlc::Disabled),
      30, uint(Vlc::Discard),
      31, uint(Vlc::Blend),
      32, uint(Vlc::Mean),
      33, uint(Vlc::Bob),
      34, uint(Vlc::Linear),
      35, uint(Vlc::X),
      36, uint(Vlc::Yadif),
      37, uint(Vlc::Yadif2x),
      38, uint(Vlc::Phospor),
      39, uint(Vlc::IVTC),
      41, uint(Vlc::PreserveAspectFit),
      42, uint(Vlc::PreserveAspectCrop),
      43, uint(Vlc::Stretch),
      45, uint(Vlc::Title),
      46, uint(Vlc::Artist),
      47, uint(Vlc::Genre),
      48, uint(Vlc::Copyright),
      49, uint(Vlc::Album),
      50, uint(Vlc::TrackNumber),
      51, uint(Vlc::Description),
      52, uint(Vlc::Rating),
      53, uint(Vlc::Date),
      54, uint(Vlc::Setting),
      55, uint(Vlc::URL),
      56, uint(Vlc::Language),
      57, uint(Vlc::NowPlaying),
      58, uint(Vlc::Publisher),
      59, uint(Vlc::EncodedBy),
      60, uint(Vlc::ArtworkURL),
      61, uint(Vlc::TrackID),
      63, uint(Vlc::TS),
      64, uint(Vlc::PS),
      65, uint(Vlc::MP4),
      66, uint(Vlc::OGG),
      67, uint(Vlc::AVI),
      69, uint(Vlc::DefaultPlayback),
      70, uint(Vlc::Loop),
      71, uint(Vlc::Repeat),
      73, uint(Vlc::Original),
      74, uint(Vlc::Ignore),
      75, uint(Vlc::R_16_9),
      76, uint(Vlc::R_16_10),
      77, uint(Vlc::R_185_100),
      78, uint(Vlc::R_221_100),
      79, uint(Vlc::R_235_100),
      80, uint(Vlc::R_239_100),
      81, uint(Vlc::R_4_3),
      82, uint(Vlc::R_5_4),
      83, uint(Vlc::R_5_3),
      84, uint(Vlc::R_1_1),
      86, uint(Vlc::NoScale),
      87, uint(Vlc::S_1_05),
      88, uint(Vlc::S_1_1),
      89, uint(Vlc::S_1_2),
      90, uint(Vlc::S_1_3),
      91, uint(Vlc::S_1_4),
      92, uint(Vlc::S_1_5),
      93, uint(Vlc::S_1_6),
      94, uint(Vlc::S_1_7),
      95, uint(Vlc::S_1_8),
      96, uint(Vlc::S_1_9),
      97, uint(Vlc::S_2_0),
      99, uint(Vlc::Idle),
     100, uint(Vlc::Opening),
     101, uint(Vlc::Buffering),
     102, uint(Vlc::Playing),
     103, uint(Vlc::Paused),
     104, uint(Vlc::Stopped),
     105, uint(Vlc::Ended),
     106, uint(Vlc::Error),
     108, uint(Vlc::NoVideo),
     109, uint(Vlc::MPEG2Video),
     110, uint(Vlc::MPEG4Video),
     111, uint(Vlc::H264),
     112, uint(Vlc::Theora),
     114, uint(Vlc::DirectX),
     115, uint(Vlc::Direct3D),
     116, uint(Vlc::OpenGL),
     117, uint(Vlc::DefaultVout),

       0        // eod
};

void Vlc::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Vlc::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_Vlc.data,
    qt_meta_data_Vlc,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Vlc::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Vlc::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Vlc.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int Vlc::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
