/********************************************************************************
** Form generated from reading UI file 'Player.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PLAYER_H
#define UI_PLAYER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>
#include "../../src/widgets/WidgetSeek.h"
#include "../../src/widgets/WidgetVideo.h"
#include "../../src/widgets/WidgetVolumeSlider.h"

QT_BEGIN_NAMESPACE

class Ui_Player
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout_2;
    QPushButton *stop;
    QPushButton *pause;
    VlcWidgetVideo *video;
    VlcWidgetSeek *seek;
    VlcWidgetVolumeSlider *volume;

    void setupUi(QMainWindow *Player)
    {
        if (Player->objectName().isEmpty())
            Player->setObjectName(QString::fromUtf8("Player"));
        Player->resize(640, 480);
        centralwidget = new QWidget(Player);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayout_2 = new QGridLayout(centralwidget);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        stop = new QPushButton(centralwidget);
        stop->setObjectName(QString::fromUtf8("stop"));

        gridLayout_2->addWidget(stop, 2, 1, 1, 1);

        pause = new QPushButton(centralwidget);
        pause->setObjectName(QString::fromUtf8("pause"));
        pause->setCheckable(true);

        gridLayout_2->addWidget(pause, 2, 0, 1, 1);

        video = new VlcWidgetVideo(centralwidget);
        video->setObjectName(QString::fromUtf8("video"));

        gridLayout_2->addWidget(video, 0, 0, 1, 2);

        seek = new VlcWidgetSeek(centralwidget);
        seek->setObjectName(QString::fromUtf8("seek"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(seek->sizePolicy().hasHeightForWidth());
        seek->setSizePolicy(sizePolicy);

        gridLayout_2->addWidget(seek, 4, 0, 1, 2);

        volume = new VlcWidgetVolumeSlider(centralwidget);
        volume->setObjectName(QString::fromUtf8("volume"));
        sizePolicy.setHeightForWidth(volume->sizePolicy().hasHeightForWidth());
        volume->setSizePolicy(sizePolicy);

        gridLayout_2->addWidget(volume, 3, 0, 1, 2);

        Player->setCentralWidget(centralwidget);

        retranslateUi(Player);

        QMetaObject::connectSlotsByName(Player);
    } // setupUi

    void retranslateUi(QMainWindow *Player)
    {
        Player->setWindowTitle(QCoreApplication::translate("Player", "Basic Player Test", nullptr));
        stop->setText(QCoreApplication::translate("Player", "Stop", nullptr));
        pause->setText(QCoreApplication::translate("Player", "Pause", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Player: public Ui_Player {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PLAYER_H
