INCLUDE("E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/config/VLCQtCoreTargets.cmake")

IF(NOT STATIC AND Windows MATCHES "Darwin")
    SET(VLCQtCoreFramework E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/config/../../VLCQtCore.framework)
ELSE()
    GET_TARGET_PROPERTY(VLCQtCoreLocation VLCQt::Core INTERFACE_INCLUDE_DIRECTORIES)
    STRING(REGEX REPLACE "/include" "" VLCQtCoreLocation "")
ENDIF()
