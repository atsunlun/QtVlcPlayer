INCLUDE(CMakeFindDependencyMacro)
FIND_DEPENDENCY(VLCQtCore)

INCLUDE("E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/config/VLCQtQmlTargets.cmake")

SET_TARGET_PROPERTIES(VLCQt::Qml PROPERTIES
    INTERFACE_LINK_LIBRARIES VLCQt::Core
)

IF(NOT STATIC AND Windows MATCHES "Darwin")
    SET(VLCQtQmlFramework E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/config/../../VLCQtQml.framework)
    SET(VLCQtQmlPluginDir E:/Work/GitCode/HanLin_Code/HLClassSoft/src/VLC/vlc-qt-1.1.1/config/../../../qml/VLCQt)
ELSE()
    GET_TARGET_PROPERTY(VLCQtQmlLocation VLCQt::Qml INTERFACE_INCLUDE_DIRECTORIES)
    STRING(REGEX REPLACE "/include" "" VLCQtQmlLocation "")
ENDIF()
